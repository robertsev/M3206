#!/bin/bash

dossier="sauvegarde"
date_backup=$(date +%Y_%m_%d-%H%M)

nom=$(basename $1)
fichier=$(echo tar cvfj $date_backup"_"$nom".tar.gz")

