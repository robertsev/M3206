#bin/bash
if dpkg -V git; then #verifie si le paquet est installé
echo "[...]git:installe[...]"
else
echo "lancer la commande: apt-get install git"
fi

if dpkg -V tmux; then
echo "[...]tmux:installe[...]"
echo "lancer la commande: apt-get install tmux"
fi

if dpkg -V vim; then
echo "[...]vimx:installe[...]"
else 
echo "lancer la commande: apt-get install vim"
fi

if dpkg -V htop; then
echo "[...]htop:installe[...]"
else
echo "lancer la commande: apt-get install htop"
fi
